window.addEventListener("DOMContentLoaded", async () => {
    const locationsApiUrl = "http://localhost:8000/api/locations/";

    const response = await fetch(locationsApiUrl);
    if (response.ok) {
        const data = await response.json();

        const locSelectTag = document.getElementById("location");
        for (const location of data.locations) {
            const locOptionTag = document.createElement("option");
            locOptionTag.value = location.id;
            locOptionTag.innerHTML = location.name;
            locSelectTag.appendChild(locOptionTag);
        }
    }

    const newConfForm = document.getElementById("create-conference-form");
    newConfForm.addEventListener("submit", async (event) => {
        event.preventDefault();

        const formData = new FormData(newConfForm);
        const formJson = JSON.stringify(Object.fromEntries(formData));
        console.log(formJson);
        const fetchOptions = {
            method: "POST",
            body: formJson,
            headers: {"Content-Type": "applicaton/json"}
        }

        const conferencesApiURL = "http://localhost:8000/api/conferences/";
        const submitResp = await fetch(conferencesApiURL, fetchOptions);
        if (submitResp.ok) {
            newConfForm.reset();
            // const newConf = await submitResp.json();
            // console.log(newConf);
        }

    });

});
