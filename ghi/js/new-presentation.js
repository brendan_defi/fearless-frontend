window.addEventListener("DOMContentLoaded", async () => {
    const conferencesApiURL = "http://localhost:8000/api/conferences/";

    const response = await fetch(conferencesApiURL);
    if (response.ok) {
        const data = await response.json();

        const confSelectTag = document.getElementById("conference");
        for (const conference of data.conferences) {
            const confOptionTag = document.createElement("option");
            confOptionTag.value = conference.id;
            confOptionTag.innerHTML = conference.name;
            confSelectTag.appendChild(confOptionTag);
        }
    }


    const newPresoForm = document.querySelector("#create-presentation-form");
    newPresoForm.addEventListener("submit", async (event) => {
        event.preventDefault();

        const formData = new FormData(newPresoForm);
        const formJson = JSON.stringify(Object.fromEntries(formData));
        const fetchOptions = {
            method: "POST",
            body: formJson,
            headers: {"Content-Type": "applicaton/json"},
        };

        const confId = formData.get("conference")
        const presentationsUrl = `http://localhost:8000/api/conferences/${confId}/presentations/`;
        const submitResp = await fetch(presentationsUrl, fetchOptions);
        if (submitResp.ok) {
            newPresoForm.reset();
            const newPreso = await submitResp.json();
            console.log(newPreso);
        }

    });

});
