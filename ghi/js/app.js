const createCard = (name, description, pictureUrl, start, end, location) => {
    const newCard = document.createElement("div");
    newCard.className = "col-4";
    newCard.innerHTML = `
            <div class="card shadow">
                <img src="${pictureUrl}" class="card-img-top" alt="..." style="max-height: 200px;">
                <div class="card-body">
                    <h5 class="card-title">${name}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                    <p class="card-text">${description.length > 280 ? description.substring(0,140) + "..." : description}</p>
                </div>
                <div class="card-footer">
                    ${start} - ${end}
                </div>
            </div>
    `
    return newCard;
}


const createPlaceholderCard = () => {
    return `
                    <div class="col-4 placeholder-card">
                        <div class="card shadow">
                            <svg class="bd-placeholder-img card-img-top" width="100%" height="180" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder" preserveAspectRatio="xMidYMid slice" focusable="false">
                                <title>Placeholder</title>
                                <rect width="100%" height="100%" fill="#868e96"></rect>
                            </svg>
                            <div class="card-body">
                                <h5 class="card-title placeholder-glow">
                                    <span class="placeholder col-6"></span>
                                </h5>
                                <h6 class="card-subtitle placeholder-glow">
                                    <span class="placeholder col-4"></span>
                                </h6>
                                <p class="card-text placeholder-glow">
                                    <span class="placeholder col-7"></span>
                                    <span class="placeholder col-4"></span>
                                    <span class="placeholder col-8"></span>
                                    <span class="placeholder col-2"></span>
                                    <span class="placeholder col-5"></span>
                                </p>
                            </div>
                            <div class="card-footer placeholder-glow">
                                <span class="placeholder col-6"></span>
                            </div>
                        </div>
                    </div>
                `
}


const createErrorAlert = (e="") => {
    const errHtml = `
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Error fetching conference data</strong> ${e}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    `;
    const errbar = document.querySelector(".api-error-bar");
    errbar.innerHTML = errHtml;
    return errbar;
}


window.addEventListener('DOMContentLoaded', async () => {

    const url = "http://localhost:8000/api/conferences/"
    try {
        // pull the list of conferences
        const response = await fetch(url);
        if (!response.ok) {
            createErrorAlert();
        }
        else {
            // pull the data from the promise
            const data = await response.json();

            const numConferences = data.conferences.length

            for (let i = 0; i < numConferences; i++) {
                const placeholderHtml = createPlaceholderCard();
                const row = document.querySelector(".row");
                row.innerHTML += placeholderHtml;
            }

            for (const conf of data.conferences) {
                // pull the detail of the conf
                const detailUrl = `http://localhost:8000/${conf.href}`;
                const detailResponse = await fetch(detailUrl);
                if (!detailResponse.ok) {
                    createErrorAlert();
                }
                else {
                    // pull the details data from the promise
                    const detailData = await detailResponse.json();
                    const confName = detailData.conference.name;
                    const confDesc = detailData.conference.description;
                    const confImg = detailData.conference.location.picture_url;
                    const confStart = (new Date(detailData.conference.starts)).toLocaleDateString();
                    const confEnd = (new Date(detailData.conference.ends)).toLocaleDateString();
                    const confLoc = detailData.conference.location.name;

                    // and then create the fully-loaded card HTML
                    const html = createCard(confName, confDesc, confImg, confStart, confEnd, confLoc);
                    // pull the placeholder to be removed
                    const placeholderCard = document.querySelector(".placeholder-card")
                    // and replace the placeholder with the new card
                    placeholderCard.replaceWith(html)
                }
            }
        }
    }
    catch (e) {
        createErrorAlert(e);
    }
});
