window.addEventListener("DOMContentLoaded", async () => {
    const statesApiUrl = "http://localhost:8000/api/states/";

    const response = await fetch(statesApiUrl);
    if(response.ok) {
        const data = await response.json();

        const stateSelectTag = document.getElementById("state");
        for (const state of data.states) {
            const stateOptionTag = document.createElement("option");
            stateOptionTag.value = state.abbreviation;
            stateOptionTag.innerHTML = state.name;
            stateSelectTag.appendChild(stateOptionTag);
        }
    }

    const newLocForm = document.getElementById("create-location-form");
    newLocForm.addEventListener("submit", async (event) => {
        event.preventDefault();

        //  prep our form data for submission
        const formData = new FormData(newLocForm);
        // console.log(formData);
        const formJson = JSON.stringify(Object.fromEntries(formData));
        // console.log(formJson);
        const fetchOptions = {
            method: "POST",
            body: formJson,
            headers: {"Content-Type": "application/json"}
        };

        const newLocUrl = "http://localhost:8000/api/locations/";
        const submitResp = await fetch(newLocUrl, fetchOptions);
        if (submitResp.ok) {
            newLocForm.reset();
            // const newLoc = await submitResp.json();
            // console.log(newLoc);
        }

    });

});
