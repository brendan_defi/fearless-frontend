import { Routes, Route, NavLink, useNavigate } from "react-router-dom";

import Nav from "./Nav.js";
import AttendeesList from "./AttendeesList.js";
import LocationForm from "./LocationForm.js";
import ConferenceForm from "./ConferenceForm.js";
import AttendConferenceForm from "./AttendConferenceForm.js";
import PresentationForm from "./PresentationForm.js";
import MainPage from "./MainPage.js";

export default function App() {

  return (
    <>
      <Nav />
      <div className="container">
        <Routes>
          <Route index="true" element={<MainPage />}></Route>
          <Route path="attendees" element={<AttendeesList />}></Route>
          <Route path="new" element={<AttendConferenceForm />} ></Route>
          <Route path="conferences/new" element={<ConferenceForm />}></Route>
          <Route path="locations/new" element={<LocationForm />}></Route>
          <Route path="presentations/new" element={<PresentationForm />}></Route>
        </Routes>
      </div>
    </>
  );
}
