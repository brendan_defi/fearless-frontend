import React, { useEffect , useState } from "react";

export default function LocationForm() {
    const [states, setStates] = useState([]);
    const [nameInput, setNameInput] = useState("");
    const [roomCountInput, setRoomCountInput] = useState("");
    const [cityInput, setCityInput] = useState("");
    const [stateSelection, setStateSelection] = useState("");

    const fetchStates = async () => {
        const statesApiUrl = "http://localhost:8000/api/states/";

        const response = await fetch(statesApiUrl);
        if(response.ok) {
            const data = await response.json();
            setStates(data.states);
        }
    }

    useEffect( () => {
        fetchStates();
    }, []);

    const handleNameInput = (e) => {
        setNameInput(e.target.value);
    }

    const handleRoomCountInput = (e) => {
        setRoomCountInput(e.target.value);
    }

    const handleCityInput = (e) => {
        setCityInput(e.target.value);
    }

    const handleStateInput = (e) => {
        setStateSelection(e.target.value);
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        const submissionData = {
            name: nameInput,
            room_count: roomCountInput,
            city: cityInput,
            state: stateSelection,
        };

        const fetchOptions = {
            method: "POST",
            body: JSON.stringify(submissionData),
            headers: {"Content-Type": "application/json"}
        };

        const newLocUrl = "http://localhost:8000/api/locations/";
        const submitResp = await fetch(newLocUrl, fetchOptions);
        if (submitResp.ok) {
            setNameInput("");
            setCityInput("");
            setRoomCountInput("");
            setStateSelection("");
        }

    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new location</h1>
                    <form onSubmit={handleSubmit} id="create-location-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameInput} value={nameInput} placeholder="Name" required type="text" id="name" name="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleRoomCountInput} value={roomCountInput} placeholder="Room count" required type="number" id="room_count" name="room_count" className="form-control" />
                            <label htmlFor="room_count">Room count</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleCityInput} value={cityInput} placeholder="City" required type="text" id="city" name="city" className="form-control" />
                            <label htmlFor="city">City</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleStateInput} value={stateSelection} required id="state" name="state" className="form-select">
                            <option value="">Choose a state</option>
                            {states.map( (state) => {
                                return (
                                    <option key={state.abbreviation} value={state.abbreviation}>
                                        {state.name}
                                    </option>
                                );
                            })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
