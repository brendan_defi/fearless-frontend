import React, { useEffect, useState } from "react"

export default function AttendConferenceForm() {
    const [conferences, setConferences] = useState([]);
    const [nameInput, setNameInput] = useState("");
    const [emailInput, setEmailInput] = useState("");
    const [conferenceSelection, setConferenceSelection] = useState("");


    const fetchConferences = async () => {
        const confApiUrl = "http://localhost:8000/api/conferences/";
        const response = await fetch(confApiUrl);
        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences);
        }
    }

    useEffect( () => {
        fetchConferences();
    }, []);

    const handleConferenceInput = (e) => {
        setConferenceSelection(e.target.value);
    }

    const handleNameInput = (e) => {
        setNameInput(e.target.value);
    }

    const handleEmailInput = (e) => {
        setEmailInput(e.target.value);
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        const submissionData = {
            name: nameInput,
            email: emailInput,
        };
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(submissionData),
            headers: {"Content-Type": "application/json"},
        };

        const attendeesApiUrl = `http://localhost:8001${conferenceSelection}attendees/`;
        const submitResp = await fetch(attendeesApiUrl, fetchConfig);
        if (submitResp.ok) {
            setConferenceSelection("");
            setNameInput("");
            setEmailInput("");
        }
    }

    return (
        <div className="container d-flex justify-content-center align-items-start">
            <div className="row m-4 bg-white shadow col-4">
                <img src="/logo.svg" alt="" style={{maxWidth: "400px", height: "auto"}}/>
            </div>
            <div className="row col-8">
                <div className="shadow p-4 mt-4">
                    <h1>It's Conference Time!</h1>
                    <form onSubmit={handleSubmit} id="book-conference-form">
                        <p>Please choose which conference you'd like to attend.</p>
                        <div className="mb-3">
                            <select onChange={handleConferenceInput} value={conferenceSelection} required id="location" name="location" className="form-select">
                                <option defaultValue value="">Choose a conference</option>
                                {conferences.map( conference => {
                                    return (
                                        <option key={conference.href} value={conference.href}>
                                            {conference.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <p>Now, tell us about yourself.</p>
                        <div className="form-floating mb-3">
                            <input onChange={handleNameInput} value={nameInput} placeholder="Name" required type="text" id="name" name="name" className="form-control" />
                            <label htmlFor="name">Your full name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEmailInput} value={emailInput} placeholder="Email" required type="text" id="email" name="email" className="form-control" />
                            <label htmlFor="email">Your email address</label>
                        </div>
                        <button className="btn btn-primary">Book me a spot!</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
