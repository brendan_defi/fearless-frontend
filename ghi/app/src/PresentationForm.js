import React, { useState, useEffect } from "react";

export default function PresentationForm() {
    const [conferences, setConferences] = useState([]);
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [companyName, setCompanyName] = useState("");
    const [title, setTitle] = useState("");
    const [synopsis, setSynopsis] = useState("");
    const [conferenceSelection, setConferenceSelection] = useState("");

    const fetchConferences = async () => {
        const confApiUrl = "http://localhost:8000/api/conferences/";
        const response = await fetch(confApiUrl);
        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences);
        }
    }

    useEffect( () => {
        fetchConferences();
    }, []);

    const handleName = (e) => {
        setName(e.target.value);
    }

    const handleEmail = (e) => {
        setEmail(e.target.value);
    }

    const handleCompanyName = (e) => {
        setCompanyName(e.target.value);
    }

    const handleTitle = (e) => {
        setTitle(e.target.value);
    }

    const handleSynopsis = (e) => {
        setSynopsis(e.target.value);
    }

    const handleConference = (e) => {
        setConferenceSelection(e.target.value);
    }

    const handleSubmission = async (e) => {
        e.preventDefault();

        const submissionData = {
            presenter_name: name,
            presenter_email: email,
            company_name: companyName,
            title: title,
            synopsis: synopsis,
            conference: conferenceSelection
        }

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(submissionData),
            headers: {"Content-Type": "application/json"}
        }

        const presentationsApiUrl =`http://localhost:8000${conferenceSelection}presentations/`;
        const submitResp = await fetch(presentationsApiUrl, fetchConfig);
        if (submitResp.ok) {
            setName("");
            setEmail("");
            setCompanyName("");
            setTitle("");
            setSynopsis("");
            setConferenceSelection("");
        }
    }


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create new presentation</h1>
                    <form onSubmit={handleSubmission} id="create-presentation-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleName} value={name} placeholder="Presenter name" required type="text" id="presenter_name" name="presenter_name" className="form-control" />
                                <label htmlFor="presenter_name">Presenter name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEmail} value={email} placeholder="Presenter email" required type="text" id="presenter_email" name="presenter_email" className="form-control" />
                                <label htmlFor="presenter_email">Presenter email</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleCompanyName} value={companyName} placeholder="Company name" required type="text" id="company_name" name="company_name" className="form-control" />
                                <label htmlFor="company_name">Company name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleTitle} value={title} placeholder="Title" required type="text" id="title" name="title" className="form-control" />
                                <label htmlFor="title">Title</label>
                        </div>
                        <div className="form mb-3">
                            <label htmlFor="Synopsis">Synopsis</label>
                            <textarea onChange={handleSynopsis} value={synopsis} placeholder="Tell us about your presentation" required type="text" id="synopsis" name="synopsis" className="form-control" style={{minHeight: "100px"}}></textarea>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleConference} value={conferenceSelection} required id="conference" name="conference" className="form-select">
                                <option defaultValue value="">Choose a conference</option>
                                {conferences.map( conference => {
                                    return (
                                        <option key={conference.href} value={conference.href}>
                                            {conference.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
