import React, { useEffect, useState } from "react"

export default function ConferenceForm() {
    const [locations, setLocations] = useState([]);

    const [formData, setFormData] = useState({
        name: "",
        starts: "",
        ends: "",
        description: "",
        max_presentations: "",
        max_attendees: "",
        location: "",
    });


    // const [nameInput, setNameInput] = useState("");
    // const [startDateInput, setStartDateInput] = useState("");
    // const [endDateInput, setEndDateInput] = useState("");
    // const [descriptionInput, setDescriptionInput] = useState("");
    // const [maxPresentationsInput, setMaxPresentationsInput] = useState("");
    // const [maxAttendeesInput, setMaxAttendeesInput] = useState("");
    // const [locationSelection, setLocationSelection] = useState("");

    const fetchLocations = async () => {
        const locationsApiUrl = "http://localhost:8000/api/locations/";

        const response = await fetch(locationsApiUrl);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    }

    useEffect( () => {
        fetchLocations();
    }, []);

    // const handleNameInput = (e) => {
    //     setNameInput(e.target.value);
    // }

    // const handleStartDateInput = (e) => {
    //     setStartDateInput(e.target.value);
    // }

    // const handleEndDateInput = (e) => {
    //     setEndDateInput(e.target.value);
    // }

    // const handleDescriptionInput = (e) => {
    //     setDescriptionInput(e.target.value);
    // }

    // const handleMaxPresentationsInput = (e) => {
    //     setMaxPresentationsInput(e.target.value);
    // }

    // const handleMaxAttendeesInput = (e) => {
    //     setMaxAttendeesInput(e.target.value);
    // }

    // const handleLocationInput = (e) => {
    //     setLocationSelection(e.target.value);
    // }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        // const submissionData = {
        //     name: nameInput,
        //     starts: startDateInput,
        //     ends: endDateInput,
        //     description: descriptionInput,
        //     max_presentations: maxPresentationsInput,
        //     max_attendees: maxAttendeesInput,
        //     location: locationSelection,
        // };
        // console.log(submissionData);

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {"Content-Type": "application/json"},
        };
        console.log(fetchConfig);

        const confApiUrl = "http://localhost:8000/api/conferences/";
        const submitResp = await fetch(confApiUrl, fetchConfig);
        if (submitResp.ok) {
            setFormData({
                name: "",
                starts: "",
                ends: "",
                description: "",
                max_presentations: "",
                max_attendees: "",
                location: "",
            });

            // setNameInput("");
            // setStartDateInput("");
            // setEndDateInput("");
            // setDescriptionInput("");
            // setMaxPresentationsInput("");
            // setMaxAttendeesInput("");
            // setLocationSelection("");
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new conference</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.name} placeholder="Name" required type="text" id="name" name="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.starts} placeholder="Start date" required type="date" id="starts" name="starts" className="form-control" />
                            <label htmlFor="starts">Start date</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.ends} placeholder="End date" required type="date" id="ends" name="ends" className="form-control" />
                            <label htmlFor="ends">End date</label>
                        </div>
                        <div className="form mb-3">
                            <label htmlFor="Description">Description</label>
                            <textarea onChange={handleFormChange} value={formData.description} placeholder="Tell us about your conference" required type="text" id="description" name="description" className="form-control" style={{minHeight: "100px"}}></textarea>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.max_presentations} placeholder="Max Presentations" required type="number" min="0" id="max_presentations" name="max_presentations" className="form-control" />
                            <label htmlFor="max_presentations">Max Presentations</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.max_attendees} placeholder="Max Attendees" required type="number" min="0" id="max_attendees" name="max_attendees" className="form-control" />
                            <label htmlFor="max_attendees">Max Attendees</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleFormChange} value={formData.location} required id="location" name="location" className="form-select">
                                <option defaultValue value="">Choose a location</option>
                                {locations.map( location => {
                                    return (
                                        <option key={location.id} value={location.id}>
                                            {location.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
