import {useState, useEffect } from "react";
// import { Outlet } from "react-router-dom";

export default function AttendeesList(props) {
    const [attendees, setAttendees] = useState([]);

    const fetchAttendees = async () => {
        const response = await fetch('http://localhost:8001/api/attendees/');
        if (response.ok) {
            const data = await response.json();

            if (data === undefined) {
                return null;
            }
            else {
                setAttendees(data.attendees);
            }
        }
        else {
            console.error(response);
        }
    }

    useEffect( () => {
        fetchAttendees();
    }, [])

    return (
        <div className="container">
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Conference</th>
            </tr>
            </thead>
            <tbody>
                {attendees.map(attendee => {
                    return (
                    <tr key={ attendee.href }>
                        <td>{ attendee.name }</td>
                        <td>{ attendee.conference }</td>
                    </tr>
                    );
                })}
            </tbody>
        </table>
        {/* <Outlet /> */}
        </div>
    );
}
