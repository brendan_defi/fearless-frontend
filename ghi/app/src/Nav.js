import { NavLink } from "react-router-dom";

export default function Nav() {

    return (
    <header>
        <div className="api-error-bar"></div>
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="container-fluid">
                <a className="navbar-brand" href="#">Conference GO!</a>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                    <NavLink
                        to='/'
                        className="nav-link"
                        style={(routerProps) => {
                            const { isActive } = routerProps

                            return {
                                fontWeight: isActive ? 'bold' : 'normal',
                                textDecoration: isActive ? 'underline' : 'none',
                            }
                        }}
                    >
                        <span>Home</span>
                    </NavLink>
                    <NavLink
                        to='/locations/new'
                        className="nav-link"
                        style={(routerProps) => {
                            const { isActive } = routerProps

                            return {
                                fontWeight: isActive ? 'bold' : 'normal',
                                textDecoration: isActive ? 'underline' : 'none',
                            }
                        }}
                    >
                        <span>New location</span>
                    </NavLink>
                    <NavLink
                        to='/conferences/new'
                        className="nav-link"
                        style={(routerProps) => {
                            const { isActive } = routerProps

                            return {
                                fontWeight: isActive ? 'bold' : 'normal',
                                textDecoration: isActive ? 'underline' : 'none',
                            }
                        }}
                    >
                        <span>New conference</span>
                    </NavLink>
                    <NavLink
                        to='/presentations/new'
                        className="nav-link"
                        style={(routerProps) => {
                            const { isActive } = routerProps

                            return {
                                fontWeight: isActive ? 'bold' : 'normal',
                                textDecoration: isActive ? 'underline' : 'none',
                            }
                        }}
                    >
                        <span>New presentation</span>
                    </NavLink>
                </ul>
                </div>
            </div>
        </nav>
    </header>
    );
}
